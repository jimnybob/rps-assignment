name := "rps-assigment"

version := "1.0"

resolvers +=
  "mvn-neo4j" at "http://m2.neo4j.org/content/groups/everything"

libraryDependencies += "org.neo4j" % "neo4j" % "2.1.0-M01" classifier "docs"

libraryDependencies += "org.neo4j" % "neo4j-rest-graphdb" % "2.0.1"

libraryDependencies += "com.sun.jersey" % "jersey-core" % "1.9"