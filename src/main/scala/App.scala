import java.util.Date
import org.neo4j.graphdb.{Node => NeoNode, DynamicRelationshipType, Transaction, ResourceIterable, DynamicLabel}
import scala.xml.Node

import scala.collection.JavaConversions._

import org.neo4j.rest.graphdb.RestGraphDatabase
import java.util.Date

/**
 * This is a quick, dirty, imperative, non object-oriented class
 *
 * Created by James on 02/04/2014.
 */
object App {

  case class Interaction(identity_number: String, interactants: Seq[Interactant], msgText: String, actionTxt: String, severity: Int)

  case class Interactant(name: String, mart_id: String, route: String)

  case class Section(sectype: String, modate: Date, id_number: String, heading: String, synonyms: Seq[Synonym], atcCodes: Seq[Code], category: String, summary: String)

  case class Synonym(term: String, lang: String)

  case class Code(code: String)

  private lazy val graphDb: RestGraphDatabase = new RestGraphDatabase("http://localhost:7474/db/data/");

  def main(args: Array[String]) {


    val intrcs: Seq[Interaction] = getInteractions()
    println(intrcs.mkString("\n"))
    val sections: List[Section] = List(getSection("11415-z.xml"), getSection("22571-v.xml"))
    println(sections.mkString("\n"))

    intrcs.foreach {
      intr =>
        var intrNode: Option[NeoNode] = nodeExists("Interaction", "identity_number", intr.identity_number)
        if (intrNode.isEmpty) {
          intrNode = Some(createInteractionNode(intr))
        }
        intr.interactants.foreach {
          intract =>
            var intractNode: Option[NeoNode] = nodeExists("Interactant", "mart_id", intract.mart_id)
            if (intractNode.isEmpty) {
              intractNode = Some(createInteractantNode(intract))
            }

            createDependencyRelationship(intrNode.get, intractNode.get, "interacts_with")
        }
    }

    sections.foreach {
      sec =>
        var secNode: Option[NeoNode] = nodeExists("Section", "id_number", sec.id_number)
        if (secNode.isEmpty) {
          secNode = Some(createSectionNode(sec))
        }
        var intractNode: Option[NeoNode] = nodeExists("Interactant", "mart_id", sec.id_number)
        createDependencyRelationship(intractNode.get, secNode.get, "is_a")
    }
  }

  private def nodeExists(label: String, attribute: String, value: String): Option[NeoNode] = {
    val existingNodes: ResourceIterable[NeoNode] = graphDb.findNodesByLabelAndProperty(DynamicLabel.label(label), attribute, value);
    if (existingNodes.iterator().hasNext) {
      return Some(existingNodes.iterator().next())
    }

    None
  }

  private def createInteractionNode(intr: Interaction): NeoNode = {

    val tx: Transaction = graphDb.beginTx()
    try {
      val node: NeoNode = graphDb.createNode();
      node.addLabel(DynamicLabel.label("Interaction"));
      node.setProperty("identity_number", intr.identity_number);
      node.setProperty("message_text", intr.msgText);
      node.setProperty("action_text", intr.actionTxt);
      node.setProperty("severity", intr.severity);
      tx.success();
      node
    } finally {
      tx.close();
    }
  }

  private def createInteractantNode(intr: Interactant): NeoNode = {

    val tx: Transaction = graphDb.beginTx()
    try {
      val node: NeoNode = graphDb.createNode();
      node.addLabel(DynamicLabel.label("Interactant"));
      node.setProperty("mart_id", intr.mart_id);
      node.setProperty("name", intr.name);
      node.setProperty("route", intr.route);
      tx.success();
      node
    } finally {
      tx.close();
    }
  }

  private def createSectionNode(section: Section): NeoNode = {

    val tx: Transaction = graphDb.beginTx()
    try {
      val node: NeoNode = graphDb.createNode();
      node.addLabel(DynamicLabel.label("Section"));
      node.setProperty("id_number", section.id_number);
      node.setProperty("heading", section.heading);
      node.setProperty("modate", section.modate);
      node.setProperty("sectype", section.sectype);
      node.setProperty("category", section.category);
      tx.success();
      node
    } finally {
      tx.close();
    }
  }

  private def createDependencyRelationship(classNode: NeoNode, dependencyNode: NeoNode, dynamicRelationship: String): Unit = {

    val tx: Transaction = graphDb.beginTx()
    try {
      classNode.createRelationshipTo(dependencyNode, DynamicRelationshipType.withName(dynamicRelationship));
      tx.success();
    } finally {
      tx.close()
    }
  }

  /**
   * Deserialise XML
   * @param filename
   * @return
   */
  private def getSection(filename: String): Section = {
    val section = scala.xml.XML.load(App.getClass.getClassLoader.getResourceAsStream(filename))
    val idNum = (section \ "id_number").text
    val secType = (section \ "@sectype").text
    val moDate = (section \ "@modate").text
    val heading = (section \ "heading").text
    val synonyms = (section \ "synonyms" \ "drug_synonym").map {
      synonym =>
        val term = (synonym \ "synonym_term").text
        val lang = (synonym \ "synonym_term" \ "@lang").text
        Synonym(term, lang)
    }
    val atcCodes = (section \ "atc_codes").map {
      code =>
        Code(code.text)
    }
    val category = (section \ "category").text
    val summary = (section \ "summary").text

    var d: Date = null
    if (moDate != "") {
      d = new Date()
      d.setTime(moDate.toLong)
    }
    Section(secType, d, idNum, heading, synonyms, atcCodes, category, summary)
  }

  /**
   * Deserialise XML
   * @return
   */
  private def getInteractions(): Seq[Interaction] = {
    val interactions = scala.xml.XML.load(App.getClass.getClassLoader.getResourceAsStream("interactions.xml"))
    val intrcs = (interactions \ "interaction").map {
      interaction =>
        val idNum = (interaction \ "identity_number").text
        val interactants = (interaction \ "interactant").map {
          interactant =>
            val name = (interactant \ "name").text
            val martId = (interactant \ "mart_id").text
            val route = (interactant \ "route").text
            Interactant(name, martId, route)
        }
        val msgTxt = (interaction \ "message_text").text
        val actionTxt = (interaction \ "action_text").text
        val severity = (interaction \ "severity_code").text
        Interaction(idNum, interactants, msgTxt, actionTxt, severity.toInt)
    }

    intrcs
  }

}
